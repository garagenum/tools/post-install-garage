#!/bin/bash
# author : https://gitlab.com/Mxaxax

## CHECK ROOT
if [ "$EUID" -ne 0 ]
  then echo "Lancer le script avec sudo svp"
  exit
fi

# SYSTEM UPDATE
printf "\033[0;32m== Mise à jour du système... \033[0m\n"
apt update -y
apt upgrade -y

# FIRMWARES
update-pciids
FIRMWARES=$(whiptail --title "WIFI" --menu "Si vous rencontrer des difficultés pour vous connecter à un réseau sans-fil choisisez parmi ces choix :
Pour annuler appuyer sur esc ou echap" 15 80 5 \
  "Choix 1 :" "intel" \
  "Choix 2 :" "realtek" 3>&1 1>&2 2>&3)

if [ -z "$FIRMWARES" ]; then
  echo -e "\e[91mAucun choix"
else
  echo -e "\e[93mVous avez choisi : $FIRMWARES"
  case "$FIRMWARES" in
  "intel :")
    apt search firmware-iwlwifi
    apt install firmware-iwlwifi
    modprobe -r iwlwifi
    modprobe iwlwifi
    ;;
  "realtek :")
    apt-get install make gcc linux-headers-$(uname -r) build-essential -y
    apt install firmware-realtek -y
    whiptail --title "À propos des firmwares" --msgbox "Si vous rencontrez des problèmes pour installer le bon firmware, identifier le en entrant cette commande : lspci | egrep -i 'wifi|wireless|intel|broadcom|realtek'
    Ensuite visitez le repo https://github.com/lwfinger et cloner le projet en fonction du numero du firmware et suivez les commandes spécifiées dans le README, par EXEMPLE : 
    cd rtw88
    make
    make install
    modprobe rtw88" 12 200
    ;;
  *)
    echo -e "\e[91mUnsupported item $FIRMWARES!" >&2
    exit 1
    ;;
  esac
fi
# INTERNET ACCESS CONTROL (Blocking malicious sites)
echo $'[main]\ndns=none' > /etc/NetworkManager/conf.d/90-dns-none.conf &>/dev/null
systemctl reload NetworkManager
sed -i '1,4 s/^/#/' /etc/resolv.conf && sed -i -e '$anameserver 1.1.1.2\nnameserver 2606:4700:4700::1112' /etc/resolv.conf
systemctl reload NetworkManager

# CHECK EXISITING PROGAMS
printf "\033[0;32m== Recherche de programmes existants... \033[0m\n"
if command -v "wget","zip","bash-completion","gnome-tweaks","snapd","resolvconf","flatpak" >/dev/null 2>&1; then
  echo -e "\e[93mTous les programmes sont à jour"
else
  echo -e "\e[32mInstallation des programmes..."
  apt install wget zip unzip bash-completion gnome-tweaks snapd flatpak gnome-software-plugin-flatpak -y
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
fi
## START SNAP SERVICE
systemctl start snapd.service

# CHECK EXISTING USER
printf "\033[32;5m== Recherche d'utilisateurs existants...\033[0m\n"
## CONDITION : VISITEUR
if id "visiteur" >/dev/null 2>&1; then
  echo -e "\e[93ml'utilisateur visiteur existe déjà"
else
  adduser visiteur --gecos "visiteur,RoomNumber,WorkPhone,HomePhone" --disabled-password
  echo "visiteur:visiteur" | chpasswd
fi
## CONDITION : BELLINUXIEN
if id "bellinuxien" >/dev/null 2>&1; then
  echo -e "\e[93ml'utilisateur bellinuxien existe déjà"
else
  adduser bellinuxien --gecos "bellinuxien,RoomNumber,WorkPhone,HomePhone" --disabled-password
  echo "bellinuxien:bellinux@dm!" | chpasswd
  usermod -aG bellinuxien
  cat /var/lib/AccountsService/users/"$USER" > /var/lib/AccountsService/users/bellinuxien && sed -i 's/false/true/g' /var/lib/AccountsService/users/bellinuxien &>/dev/null
fi

# CHANGE SOURCES.LIST
printf "\033[0;32m== Modification de sources.list... \033[0m\n"
mv /etc/apt/sources.list /etc/apt/sources.list.bak
cp -r files/sources.list /etc/apt/sources.list

# CHANGE BASHRC
printf "\033[0;32m== Modification de bashrc... \033[0m\n"
mv /home/"$USER"/.bashrc /home/"$USER"/.bashrc.bak
cp -r files/.bashrc /home/"$USER"/.bashrc
source /home/"$USER"/.bashrc

# PROFIL CHOICE
printf "\033[0;32m== Choix du profil... \033[0m\n"
PROFIL=$(whiptail --separate-output --title "Profils" --checklist \
  "Quels profils d'utilisateurs sur cet ordinateur?" 15 90 10 \
  "mate-desktop" "pour les utilisateurs de l'environnement de bureau Mate" OFF \
  "kde-desktop" "pour les utilisateurs de l'environnement de bureau Kde" OFF \
  "gnome-desktop" "environnement de bureau moderne" OFF \
  "Enfants" "Des jeux et des outils éducatifs" OFF \
  "Seniors" "Des outils d'accessibilité" OFF \
  "Gamer" "Des jeux, des jeux ,des jeux" OFF \
  "MAO" "Des logiciels pour la création musicale" OFF \
  "Team-Working" "Des logiciels pour le travail collaboratif" ON \
  "DevOps" "Des logiciels pour s'initier à l'approche DevOps" OFF \
  "Toshiba_toughbook" "Correction de bug pour le son" OFF 3>&1 1>&2 2>&3)

if [ -z "$PROFIL" ]; then
  echo -e "\e[91mAucun profil n'a été choisi"
else
  for i in $PROFIL; do
    echo -e "\e[93mVous avez choisi le profil : $PROFIL"
    case "$i" in
    "mate-desktop")
      apt install gnome-system-tools blueman gnome-software -y && apt install tasksel && tasksel && sed -i 's/XSession=/XSession=mate/g' /var/lib/AccountsService/users/"$USER" && apt install ttf-mscorefonts-installer -y && fc-cache -f -v
      reboot
      ;;
    "kde-desktop")
      apt install tasksel && tasksel && sed -i 's/XSession=/XSession=plasma/g' /var/lib/AccountsService/users/"$USER" && apt install ttf-mscorefonts-installer -y && fc-cache -f -v
      reboot
      ;;
    "gnome-desktop")
      apt install gnome-tweak-tool gnome-screensaver arc-theme -y
      ;;
    "Enfants")
      echo $'[main]\ndns=none' | tee /etc/NetworkManager/conf.d/90-dns-none.conf
      systemctl reload NetworkManager
      sed -i '1,6 s/^/#/' /etc/resolv.conf && sed -i -e '$anameserver 1.1.1.3\nnameserver 2606:4700:4700::1113' /etc/resolv.conf
      systemctl reload NetworkManager
      snap install gcompris && apt install veyon-service tuxpaint pingus numptyphysics warmux xmoto slimevolley arduino -y
      ;;
    "Seniors")
      snap install skype prospect-mail
      ;;
    "Gamer")
      snap install discord minetest urban-terror warzone2100 flightgear stuntrally extreme-tux-racer tmnationsforever mc-installer mari0-ce liveforspeed opensurge crrcsim-simulator love && snap install savagexr --beta && snap install pixels-game --beta && apt install veyon-service -y && wget https://cdn.cloudflare.steamstatic.com/client/installer/steam.deb && apt install ./steam.deb && rm -rf ./steam.deb
      ;;
    "MAO")
      apt install playonlinux lmms audacity mixxx ardour rosegarden soundconverter qtractor -y && wget https://dl.4kdownload.com/app/4kyoutubetomp3_4.6.2-1_amd64.deb && apt install ./4kyoutubetomp3_4.6.2-1_amd64.deb && rm -rf ./4kyoutubetomp3_4.6.2-1_amd64.deb
      ;;
    "Team-Working")
      snap install signal-desktop zoom-client telegram-desktop messenger-collabee yakyak whatsdesk && snap install slack --classic cawbird --edge && apt install gimp gitso soundcgimp onverter scribus chromium chromium-l10n vlc brasero pdfarranger -y
      #echo "Les programmes suivant seront installés ? : $PROFIL"
      ;;
    "DevOps")
      mkdir ./devops && git clone https://gitlab.com/Mxaxax/installations.git devops/ && bash devops/devops.sh && rm -rf ./devops/
      ;;
    "Toshiba_toughbook")
      mkdir ./toshiba && git clone https://gitlab.com/garagenum/tools/linux/debian-customizer.git toshiba/ && mv /usr/share/pulseaudio/alsa-mixer/paths/analog-output-speaker.conf /usr/share/pulseaudio/alsa-mixer/paths/analog-output-speaker.conf.BACK && cp -r ./toshiba/custom-debian-conf/system-conf/analog-output-speaker.conf /usr/share/pulseaudio/alsa-mixer/paths/analog-output-speaker.conf
      rm -rf ./toshiba/
      ;;
    *)
      echo -e "\e[91mUnsupported item $PROFIL!" >&2
      exit 1
      ;;
    esac
  done
fi
# CHANGE THEME APPARENCE
printf "\033[0;32m== Modification du thème... \033[0m\n"
## THEME CHOICE
mkdir ./themes
THEMES=$(whiptail --title "Thème général" --menu "Choisissez votre thème parmi ces choix :" 15 60 5 \
  "Theme 1 :" "Layan-gtk-theme(recommandé)" \
  "Theme 2 :" "Vimix-gtk-themes" \
  "Theme 3 :" "Fluent-gtk-theme" \
  "Theme 4 :" "Nephrite-gtk-theme" 3>&1 1>&2 2>&3)

if [ -z "$THEMES" ]; then
  echo -e "\e[91mAucun thème n'a été choisi"
else
  echo -e "\e[93mVous avez choisi le theme : $THEMES"
  case "$THEMES" in
  "Theme 1 :")
    git clone https://github.com/vinceliuice/Layan-gtk-theme.git themes/ && bash themes/install.sh && gsettings set org.gnome.shell.extensions.user-theme name "Layan-Dark" && gsettings set org.gnome.desktop.interface gtk-theme "Layan-Dark"
    ;;
  "Theme 2 :")
    git clone https://github.com/vinceliuice/vimix-gtk-themes.git themes/ && bash themes/install.sh && gsettings set org.gnome.shell.extensions.user-theme name "Vimix-dark-doder" && gsettings set org.gnome.desktop.interface gtk-theme "Vimix-dark-doder"
    ;;
  "Theme 3 :")
    git clone https://github.com/vinceliuice/Fluent-gtk-theme.git themes/ && bash themes/install.sh && gsettings set org.gnome.shell.extensions.user-theme name "Fluent-Dark" && gsettings set org.gnome.desktop.interface gtk-theme "Fluent-Dark"
    ;;
  "Theme 4 :")
    git clone https://github.com/vinceliuice/Nephrite-gtk-theme.git themes/ && bash themes/install.sh && gsettings set org.gnome.shell.extensions.user-theme name "Nephrite-Dark" && gsettings set org.gnome.desktop.interface gtk-theme "Nephrite-Dark"
    ;;
  *)
    echo -e "\e[91mUnsupported item $THEMES!" >&2
    exit 1
    ;;
  esac
fi
rm -rf ./themes/

## PROGRESS STATUS
{
  for ((i = 0; i <= 100; i += 20)); do
    sleep 1
    echo $i
  done
} | whiptail --gauge "Veuillez patienter pendant l'installation" 6 60 0

## ICONS CHOICE
printf "\033[0;32m== Modification du thème d'icônes... \033[0m\n"
mkdir ./icons
ICONS=$(whiptail --title "Thème d'icônes" --menu "Choisissez votre thème d'icônes parmi ces choix :" 15 60 5 \
  "Theme 1 :" "Tela-icon-theme (recommandé)" \
  "Theme 2 :" "Fluent-icon-theme" \
  "Theme 3 :" "WhiteSur-icon-theme" \
  "Theme 4 :" "Qogir-icon-theme" 3>&1 1>&2 2>&3)

if [ -z "$ICONS" ]; then
  echo -e "\e[91mAucun thème d'icônes n'a été choisi"
else
  echo -e "\e[93mVous avez choisi le theme : $ICONS"
  case "$ICONS" in
  "Theme 1 :")
    git clone https://github.com/vinceliuice/Tela-icon-theme.git icons/ && bash icons/install.sh && gsettings set org.gnome.desktop.interface icon-theme "Tela-dark"
    ;;
  "Theme 2 :")
    git clone https://github.com/vinceliuice/Fluent-icon-theme.git icons/ && bash icons/install.sh && gsettings set org.gnome.desktop.interface icon-theme "Fluent-dark"
    ;;
  "Theme 3 :")
    git clone https://github.com/vinceliuice/WhiteSur-icon-theme.git icons/ && bash icons/install.sh && gsettings set org.gnome.desktop.interface icon-theme "WhiteSur-dark"
    ;;
  "Theme 4 :")
    git clone https://github.com/vinceliuice/Qogir-icon-theme.git icons/ && bash icons/install.sh && gsettings set org.gnome.desktop.interface icon-theme "Qogir-dark"
    ;;
  *)
    echo -e "\e[91mUnsupported item $ICONS!" >&2
    exit 1
    ;;
  esac
fi
rm -rf ./icons/
## PROGRESS STATUS
{
  for ((i = 0; i <= 100; i += 20)); do
    sleep 1
    echo $i
  done
} | whiptail --gauge "Veuillez patienter pendant l'installation" 6 60 0

## MESSAGE BOX
whiptail --title "À propos des thèmes" --msgbox "Vous pouvez changer de thème après redémarrage en ouvrant l'application 'Ajustements' dans la rubrique Apparence. Appuyez sur entrer pour continuer." 8 80

# FIREFOX ADDONS
printf "\033[0;32m== Téléchargement des extensions firefox depuis addons.mozilla.org... \033[0m\n"
mkdir ./addons
declare -A ADDONS
ADDONS=(
  [ublock]="https://addons.mozilla.org/firefox/downloads/file/4003969/ublock_origin-1.44.4.xpi"
  [google_translator]="https://addons.mozilla.org/firefox/downloads/file/1167275/google_translator_for_firefox-3.0.3.3.xpi"
  [languagetool]="https://addons.mozilla.org/firefox/downloads/file/3985810/languagetool-5.4.3.xpi"
  [tranquility]="https://addons.mozilla.org/firefox/downloads/file/3934978/tranquility_1-3.0.24.xpi"
)
for i in "${!ADDONS[@]}"; do
  wget "${ADDONS[$i]}" && mv ./*.xpi ./addons
done

## ADDONS INSTALLATION
## UBLOCK
mkdir /usr/share/mozilla/extensions/uBlock0@raymondhill.net
unzip -q ./addons/ublock_origin-1.44.4.xpi -d /usr/share/mozilla/extensions/uBlock0@raymondhill.net
## TRANSLATOR
mkdir /usr/share/mozilla/extensions/translator@zoli.bod
unzip -q ./addons/google_translator_for_firefox-3.0.3.3.xpi -d /usr/share/mozilla/extensions/translator@zoli.bod
## LANGUAGETOOL
mkdir /usr/share/mozilla/extensions/languagetool-webextension@languagetool.org
unzip -q ./addons/languagetool-5.4.3.xpi -d /usr/share/mozilla/extensions/languagetool-webextension@languagetool.org
## TRANQUILITY
mkdir /usr/share/mozilla/extensions/tranquility@ushnisha.com
unzip -q ./addons/tranquility_1-3.0.24.xpi -d /usr/share/mozilla/extensions/tranquility@ushnisha.com
rm -rf ./addons
# GNOME EXTENSIONS
printf "\033[0;32m== Téléchargement des extensions depuis extensions.gnome.org... \033[0m\n"
# EXTENSIONS
mkdir ./extensions
declare -A EXTENSIONS
EXTENSIONS=(
  [ding]="https://extensions.gnome.org/extension-data/dingrastersoft.com.v33.shell-extension.zip"
  [dash-to-panel]="https://extensions.gnome.org/extension-data/dash-to-paneljderose9.github.com.v42.shell-extension.zip"
  [arcmenu]="https://extensions.gnome.org/extension-data/arcmenuarcmenu.com.v13.shell-extension.zip"
  [user-theme]="https://extensions.gnome.org/extension-data/user-themegnome-shell-extensions.gcampax.github.com.v42.shell-extension.zip"
  [BingWallpaper]="https://extensions.gnome.org/extension-data/BingWallpaperineffable-gmail.com.v36.shell-extension.zip"
)
for i in "${!EXTENSIONS[@]}"; do
  wget "${EXTENSIONS[$i]}" && mv ./*.zip ./extensions
done

## CHECK EXISTING DIRECTORY
DIR="/usr/share/gnome-shell/extensions/"
if [ -d "$DIR" ]; then
  echo -e "\e[32mInstallation des fichiers de configuration dans ${DIR}..."
else
  echo -e "\e[93m${DIR} non trouvé. Création du répertoire..."
  mkdir /usr/share/gnome-shell/extensions
fi

## USER-THEME
apt install gnome-shell-extensions -y
## INSTALLATION
printf "\033[0;32m== Copie des extensions dans /usr/share/gnome-shell/extensions/...... \033[0m\n"
## DING
mkdir /usr/share/gnome-shell/extensions/ding@rastersoft.com
unzip -q ./extensions/dingrastersoft.com.v33.shell-extension.zip -d /usr/share/gnome-shell/extensions/ding@rastersoft.com/
## DASH-TO-PANEL
mkdir /usr/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com
unzip -q ./extensions/dash-to-paneljderose9.github.com.v42.shell-extension.zip -d /usr/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/
## ARC-MENU
mkdir /usr/share/gnome-shell/extensions/arcmenu@arcmenu.com
unzip -q ./extensions/arcmenuarcmenu.com.v13.shell-extension.zip -d /usr/share/gnome-shell/extensions/arcmenu@arcmenu.com/
## BING-WALLPAPER
mkdir /usr/share/gnome-shell/extensions/BingWallpaper@ineffable-gmail.com
unzip -q ./extensions/BingWallpaperineffable-gmail.com.v36.shell-extension.zip -d /usr/share/gnome-shell/extensions/BingWallpaper@ineffable-gmail.com/
rm -rf ./extensions

# DCONF - DESKTOP CONFIGURATION
printf "\033[0;32m== Configuration du bureau des applications...\033[0m\n"
rm -v /home/bellinuxien/.config/dconf/user
mkdir -p /etc/dconf/profile
mkdir -p /etc/dconf/db/local.d/
cp -r -v files/dconf-profile /etc/dconf/profile/user
cp -r -v files/gnome-profile.dump /etc/dconf/db/local.d/00-gn-desktop
dconf update

# SOFTWARES INSTALLATION
printf "\033[0;32m== Installation des logiciels : Element, VScodium...\033[0m\n"
## ELEMENT (logiciel libre de messagerie instantanée)
apt install software-properties-common apt-transport-https wget -y
wget -O- https://packages.riot.im/debian/riot-im-archive-keyring.gpg | gpg --dearmor | tee /usr/share/keyrings/riot-im-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | tee /etc/apt/sources.list.d/riot-im.list
apt update -y
apt install riot-desktop -y

## VSCODIUM (éditeur de code | fork de Visual Studio Code)
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg |
  gpg --dearmor |
  dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' |
  tee /etc/apt/sources.list.d/vscodium.list
apt update -y && apt install codium -y

# FONTS
printf "\033[0;32m== Installation des polices d'écritures...\033[0m\n"
## MICROSOFT FONTS
apt install ttf-mscorefonts-installer -y
fc-cache -f -v

## REBOOT
## MESSAGE BOX
whiptail --title "Redémarrage" --msgbox "Votre ordinateur va redémarrer automatiquement dans moin d'une 1 minute. Appuyez sur entrer pour continuer." 8 80
reboot
